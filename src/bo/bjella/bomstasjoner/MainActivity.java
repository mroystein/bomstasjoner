package bo.bjella.bomstasjoner;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	//private static final String MY_AD_UNIT_ID = "a151184f4a59158";
	//private AdView adView;
	private List<Bomstasjon> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		list = MyXMLDb.getList();

		// TextView textView = (TextView) findViewById(R.id.edit_message);
		// textView.setText("Antall bomstasjoner: " + list.size());
		ArrayAdapter<Bomstasjon> adapter = new ArrayAdapter<Bomstasjon>(this,
				android.R.layout.simple_dropdown_item_1line, list);
		AutoCompleteTextView autotextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);

		autotextView.setAdapter(adapter);
		
		autotextView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id){
				Bomstasjon b = (Bomstasjon) parent.getAdapter().getItem(position);
				//Toast.makeText(MainActivity.this, "Click :"+b.getNavn(), Toast.LENGTH_SHORT).show();
				startBomstasjonView(b.getNavn());
				
				
			}
		});


	}

	private void startBomstasjonView(String name) {
		Intent i = new Intent(this, BomsatsjonViewActivity.class);
		 i.putExtra("navn", name);
		 startActivity(i);

	}

	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public void startListView(View view) {
		Intent i = new Intent(this, ListViewActivity.class);
		startActivity(i);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_map:
			startMap(null);
			return true;
		case R.id.menu_list:
			startListView(null);
			return true;
		case R.id.menu_settings:
			Toast.makeText(this, "menu..", Toast.LENGTH_SHORT).show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void startMap(View view) {
		Intent i = new Intent(this, MapActivity.class);
		startActivity(i);
	}
	
	
}
