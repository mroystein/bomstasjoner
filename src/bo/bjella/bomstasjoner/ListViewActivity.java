package bo.bjella.bomstasjoner;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;



public class ListViewActivity extends Activity implements OnItemClickListener {

	
	  private List<Bomstasjon>list;

	  @Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_list_view);
	    ListView l = (ListView) findViewById(R.id.listview);
	    l.setOnItemClickListener(this);
	    list = MyXMLDb.getList();
	   
	    ArrayAdapter<Bomstasjon> adapter2 = new ArrayAdapter<Bomstasjon>(this,
		        android.R.layout.simple_list_item_1, list);
	  
	    l.setAdapter(adapter2);
	  }

	  @Override
	  public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu; this adds items to the action bar if it is present.
	    getMenuInflater().inflate(R.menu.activity_main, menu);
	    return true;
	  }

	  @Override
	  public boolean onOptionsItemSelected(MenuItem item) {
		  Toast.makeText(this, "Click", Toast.LENGTH_LONG).show();
	    return true;
	  }

	  public void onClick(View view) {
	    Toast.makeText(this, "Deletion undone", Toast.LENGTH_LONG).show();
	   
	  }

	@Override
	public void onItemClick(AdapterView<?> parent, View view,
		    int position, long id) {
		
		 Intent i = new Intent(this, BomsatsjonViewActivity.class);
		 i.putExtra("index", position);
		 startActivity(i);
		
	}

	  
	} 