package bo.bjella.bomstasjoner;


public class Bomstasjon {
	
	
	private String navn;
	private double takst_liten_bil;
	private double takst_stor_bil;
	private String vegnr;
	private String kommunenr;
	private double latPos;
	private double longPos;
	private boolean autopass;
	private String autopass_beskrivelse;
	
	

	public Bomstasjon(String navn, double takst_liten_bil,
			double takst_stor_bil, String vegnr, String kommunenr,
			double latPos, double longPos, boolean autopass,
			String autopass_beskrivelse) {
		super();
		this.navn = navn;
		this.takst_liten_bil = takst_liten_bil;
		this.takst_stor_bil = takst_stor_bil;
		this.vegnr = vegnr;
		this.kommunenr = kommunenr;
		this.latPos = latPos;
		this.longPos = longPos;
		this.autopass = autopass;
		this.autopass_beskrivelse = autopass_beskrivelse;
	}
	
	
	
	public Bomstasjon(String navn) {
	
		this.navn = navn;
		
	}
	
	
	public String getNavn() {
		return navn;
	}
	public void setNavn(String navn) {
		this.navn = navn;
	}
	public double getTakst_liten_bil() {
		return takst_liten_bil;
	}
	public void setTakst_liten_bil(double takst_liten_bil) {
		this.takst_liten_bil = takst_liten_bil;
	}
	public double getTakst_stor_bil() {
		return takst_stor_bil;
	}
	public void setTakst_stor_bil(double takst_stor_bil) {
		this.takst_stor_bil = takst_stor_bil;
	}
	public String getVegnr() {
		return vegnr;
	}
	public void setVegnr(String vegnr) {
		this.vegnr = vegnr;
	}
	public String getKommunenr() {
		return kommunenr;
	}
	public void setKommunenr(String kommunenr) {
		this.kommunenr = kommunenr;
	}
	public double getLatPos() {
		return latPos;
	}
	public void setLatPos(double latPos) {
		this.latPos = latPos;
	}
	public double getLongPos() {
		return longPos;
	}
	public void setLongPos(double longPos) {
		this.longPos = longPos;
	}
	public boolean isAutopass() {
		return autopass;
	}
	public void setAutopass(boolean autopass) {
		this.autopass = autopass;
	}
	public String getAutopass_beskrivelse() {
		return autopass_beskrivelse;
	}
	public void setAutopass_beskrivelse(String autopass_beskrivelse) {
		this.autopass_beskrivelse = autopass_beskrivelse;
	}

	@Override
	public String toString() {
		/*
		return "Bomstasjon [navn=" + navn + ", takst_liten_bil="
				+ takst_liten_bil + ", takst_stor_bil=" + takst_stor_bil
				+ ", vegnr=" + vegnr + ", kommunenr=" + kommunenr + ", latPos="
				+ latPos + ", longPos=" + longPos + ", autopass=" + autopass
				+ ", autopass_beskrivelse=" + autopass_beskrivelse + "]";
			return navn;*/
		return navn;
	}
}