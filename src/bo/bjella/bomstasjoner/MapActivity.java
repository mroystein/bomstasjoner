package bo.bjella.bomstasjoner;


import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentActivity implements OnInfoWindowClickListener {

	private GoogleMap mMap;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		setUpMapIfNeeded();
		
		
		
		
		
		List<Bomstasjon> list = MyXMLDb.getList();
		for(Bomstasjon b :list){
			makeMarker(b);
		}
		
		
		mMap.setOnInfoWindowClickListener(this);
		
	}
	
	private void makeMarker(Bomstasjon b){
		String s = "Pris liten: "+b.getTakst_liten_bil() +", stor: "+b.getTakst_stor_bil();
		LatLng latLng = new LatLng(b.getLatPos(), b.getLongPos());
		mMap.addMarker(new MarkerOptions()
        .position(latLng)
        .title(b.getNavn())
        .snippet(s));
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
	}
	
	private void setUpMapIfNeeded() {
		
	    // Do a null check to confirm that we have not already instantiated the map.
	    if (mMap == null) {
	    	
	    	Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.map);
	    	SupportMapFragment mapFragment = (SupportMapFragment) fragment;
	    	mMap = mapFragment.getMap();
	       // mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
	        // Check if we were successful in obtaining the map.
	        if (mMap != null) {
	            // The Map is verified. It is now safe to manipulate the map.

	        }
	    }
	}



	@Override
	public void onInfoWindowClick(Marker m) {
		
		Intent i = new Intent(this, BomsatsjonViewActivity.class);
		 i.putExtra("navn", m.getTitle());
		 startActivity(i);
		
		
	}

}
