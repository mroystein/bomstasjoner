package bo.bjella.bomstasjoner;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.util.Xml;

public class MyXMLDb extends Application{

	
	
	private static List<Bomstasjon> list;
	
	private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext(){
        return mContext;
    }
	
	public MyXMLDb() {
		
	}
	
	
	private static void fyllListe(){
		//InputStream is = mContext.getResources().openRawResource(R.raw.bomlist);
		InputStream is = mContext.getResources().openRawResource(R.raw.bomstasjoner2);
		
		
		try {
			list = parse(is);
			
			
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static String TAG = "MyXMLDB";
	public static List<Bomstasjon> getList(){
		if(list==null){
			Log.d(TAG, "listen er tom, fyller den.");
			fyllListe();
		}
		return list;
	}
	
	  // We don't use namespaces
  private static final String ns = null;
 
  public static List<Bomstasjon> parse(InputStream in) throws XmlPullParserException, IOException {
      try {
          XmlPullParser parser = Xml.newPullParser();
          parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
          parser.setInput(in, null);
          parser.nextTag();
          return readFeed(parser);
      } finally {
          in.close();
      }
  }
  
  private static List<Bomstasjon>  readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
      List<Bomstasjon>  entries = new ArrayList<Bomstasjon>();
      int teller = 0;
      parser.require(XmlPullParser.START_TAG, ns, "entries");
      while (parser.next() != XmlPullParser.END_TAG) {
          if (parser.getEventType() != XmlPullParser.START_TAG) {
              continue;
          }
          String name = parser.getName();
          teller++;
          // Starts by looking for the entry tag
          if (name.equals("entry")) {
          	
              entries.add(readEntry(parser));
          } else {
              skip(parser);
          }
      }  
      Log.d("bom","teller ="+teller);
      return entries;
  }
  
  

    
  // Parses the contents of an entry. If it encounters a title, summary, or link tag, hands them off
  // to their respective "read" methods for processing. Otherwise, skips the tag.
  private static Bomstasjon readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
      parser.require(XmlPullParser.START_TAG, ns, "entry");
      String navn = "";
     String kommunenr= "";
     String autopass_beskrivelse= "";
     String vegnr= "";
     boolean autopass = true;
     
     double takstLiten =0.0;
     double takstStor =0.0;
     double latPos =0.0;
     double longPos =0.0;
     
      
      while (parser.next() != XmlPullParser.END_TAG) {
          if (parser.getEventType() != XmlPullParser.START_TAG) {
              continue;
          }
          
          String name = parser.getName();
          if (name.equals("navn")) {
              navn = readNavn(parser);
          } else if (name.equals("lat")) {
              latPos=readLat(parser);
          } else if (name.equals("long")) {
              longPos=readLong(parser);
          }else if (name.equals("takst_liten_bil")) {
              takstLiten = readTakstLiten(parser);
          } else if (name.equals("takst_stor_bil")) {
              takstStor = readTakstStor(parser);
          } 
          else if (name.equals("kommunenr")) {
          	kommunenr=readKommunenr(parser);
          } else if (name.equals("autopass_beskrivelse")) {
          	autopass_beskrivelse=readAutopass_beskrivelse(parser);
          }  else if (name.equals("vegnr")) {
          	 vegnr=readVegnr(parser);
          } 
          else if (name.equals("autopass")) {
          	   autopass=readAutopass(parser);
         } 
       
          
          else {
              skip(parser);
          }
      }
    return new Bomstasjon(navn, takstLiten, takstStor, vegnr, kommunenr, latPos, longPos, autopass, autopass_beskrivelse);
     
  }

  // Processes title tags in the feed.
  private static  String readNavn(XmlPullParser parser) throws IOException, XmlPullParserException {
      parser.require(XmlPullParser.START_TAG, ns, "navn");
      String title = readText(parser);
      parser.require(XmlPullParser.END_TAG, ns, "navn");
      return title;
  }
  
  
  private static  boolean readAutopass(XmlPullParser parser) throws IOException, XmlPullParserException {
      parser.require(XmlPullParser.START_TAG, ns, "autopass");
      String s = readText(parser);
      parser.require(XmlPullParser.END_TAG, ns, "autopass");
      return s.equals("true");
  }
  
  private static  String readVegnr(XmlPullParser parser) throws IOException, XmlPullParserException {
      parser.require(XmlPullParser.START_TAG, ns, "vegnr");
      String s = readText(parser);
      parser.require(XmlPullParser.END_TAG, ns, "vegnr");
      return s;
  }
  
  private static  String readAutopass_beskrivelse(XmlPullParser parser) throws IOException, XmlPullParserException {
      parser.require(XmlPullParser.START_TAG, ns, "autopass_beskrivelse");
      String s = readText(parser);
      parser.require(XmlPullParser.END_TAG, ns, "autopass_beskrivelse");
      return s;
  }
  
  private static  String readKommunenr(XmlPullParser parser) throws IOException, XmlPullParserException {
      parser.require(XmlPullParser.START_TAG, ns, "kommunenr");
      String s = readText(parser);
      parser.require(XmlPullParser.END_TAG, ns, "kommunenr");
      return s;
  }
  
  private static  Double readTakstLiten(XmlPullParser parser) throws IOException, XmlPullParserException {
      parser.require(XmlPullParser.START_TAG, ns, "takst_liten_bil");
      String takstStr = readText(parser);
      double n = Double.parseDouble(takstStr);
      parser.require(XmlPullParser.END_TAG, ns, "takst_liten_bil");
      return n;
  }
  private static  Double readTakstStor(XmlPullParser parser) throws IOException, XmlPullParserException {
      parser.require(XmlPullParser.START_TAG, ns, "takst_stor_bil");
      String takstStr = readText(parser);
      double n = Double.parseDouble(takstStr);
      parser.require(XmlPullParser.END_TAG, ns, "takst_stor_bil");
      return n;
  }
  
  private static  Double readLat(XmlPullParser parser) throws IOException, XmlPullParserException {
      parser.require(XmlPullParser.START_TAG, ns, "lat");
      String s = readText(parser);
      double n = Double.parseDouble(s);
      parser.require(XmlPullParser.END_TAG, ns, "lat");
      return n;
  }
  private static  Double readLong(XmlPullParser parser) throws IOException, XmlPullParserException {
      parser.require(XmlPullParser.START_TAG, ns, "long");
      String s = readText(parser);
      double n = Double.parseDouble(s);
      parser.require(XmlPullParser.END_TAG, ns, "long");
      return n;
  }
  
  
    



  // For the tags title and summary, extracts their text values.
  private static  String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
      String result = "";
      if (parser.next() == XmlPullParser.TEXT) {
          result = parser.getText();
          parser.nextTag();
      }
      return result;
  }
  
  private static  void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
      if (parser.getEventType() != XmlPullParser.START_TAG) {
          throw new IllegalStateException();
      }
      int depth = 1;
      while (depth != 0) {
          switch (parser.next()) {
          case XmlPullParser.END_TAG:
              depth--;
              break;
          case XmlPullParser.START_TAG:
              depth++;
              break;
          }
      }
   }
	
}
