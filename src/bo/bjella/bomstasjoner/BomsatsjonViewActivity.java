package bo.bjella.bomstasjoner;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

public class BomsatsjonViewActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bomsatsjon_view);

		List<Bomstasjon> list = MyXMLDb.getList();
		Intent i = getIntent();
		int n = i.getIntExtra("index", -1);

		if (n != -1) {

			Bomstasjon b = list.get(n);
			fyllData(b);
		} else {
			String navn = i.getStringExtra("navn");
			if (navn != null) {
				for (Bomstasjon b1 : list) {
					if (b1.getNavn().equals(navn)) {
						fyllData(b1);
						return;
					}
				}
			}
		}

	}

	private void fyllData(Bomstasjon b) {
		setTitle(b.getNavn());

		TextView tv = (TextView) findViewById(R.id.bomTakstLiten);
		tv.setText(b.getTakst_liten_bil() + " kr");

		tv = (TextView) findViewById(R.id.bomTakstStor);
		tv.setText(b.getTakst_stor_bil() + " kr");

		tv = (TextView) findViewById(R.id.bomAutoPassBeskr);
		tv.setText(b.getAutopass_beskrivelse());
		
		tv = (TextView) findViewById(R.id.bomVegnr);
		tv.setText(b.getVegnr());
		
		tv = (TextView) findViewById(R.id.bomKommunenr);
		tv.setText(b.getKommunenr());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_bomsatsjon_view, menu);
		return true;
	}

}
